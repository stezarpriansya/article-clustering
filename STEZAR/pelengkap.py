import pandas as pd
import numpy as np
import os
import re
# set working directory first
cwd = os.getcwd()
os.chdir("D:\\OJT FILES\\article-clustering\\STEZAR")
import mysql.connector

def match_after(word, text):
    return re.search(r'('+word+'\s+((?:\w+(?:\s+|$)){5}))', text).group(2) if re.search(r'('+word+'\s+((?:\w+(?:\s+|$)){5}))', text) else ''

# function take 2 before word
def match_before(word, text):
    return re.search(r'((?:\S+\s+){0,5}\b'+word+')', text).group(0) if re.search(r'((?:\S+\s+){0,5}\b'+word+')', text) else ''

#-----------FULL-TEXT SEARCH WITH WHOOSH----------
raw_data = pd.read_pickle('raw_after_clean_aftertri.pkl')
raw_data = raw_data[['id', 'content']]
from whoosh.index import create_in
from whoosh.fields import *
schema = Schema(id=NUMERIC(stored=True), content=TEXT(stored=True))
if not os.path.exists("index_cleansing"):
    os.mkdir("index_cleansing")
ix = create_in("index_cleansing", schema)
writer = ix.writer()
for index, row in raw_data.iterrows():
    writer.add_document(id=row['id'], content=row['content'].replace('\n', ' '))
writer.commit()
from whoosh.qparser import QueryParser
import whoosh.scoring as scoring
searcher = ix.searcher()
query2 = QueryParser("content", ix.schema).parse("*bearingatau*")
results2 = searcher.search(query2, limit=None)
results2
#---------------END OF FULL TEXT SEARCH-------------------

# init connection with mysql host
# con1 = mysql.connector.connect(user='root', password='', host='127.0.0.1', database='news_db')
con2 = mysql.connector.connect(user='root', password='', host='127.0.0.1', database='kamus')
# raw_data = pd.read_sql('SELECT id, content FROM article', con1)
# raw_data.shape
# mapped_kata = pd.read_sql('SELECT `id`, `key` FROM mapped_kata where example is null', con2)
# mapped_kata = pd.read_sql("SELECT `id`, `key` FROM mapped_kata where example like ' '", con2)
mapped_kata = pd.read_sql("SELECT * FROM `mapped_kata` where length(example) > 100", con2)
# INIT INDEXED NEWS DB
import whoosh.index as index
from whoosh.qparser import QueryParser
ix = index.open_dir("index")
searcher = ix.searcher()
query_parser = QueryParser("content", ix.schema)
for index, row in mapped_kata.iterrows():
    # print(row['key'])
    query = query_parser.parse(row['key'])
    results = searcher.search(query, limit=1)
    banyak = results.estimated_length()
    if banyak != 0:
        example = results[0]['content'].lower()
        bef = match_before(row['key'], example)
        aft = match_after(row['key'], example)
        if (bef != '') or (aft != ''):
            example = bef+' '+aft

        example = re.sub(r'[^a-zA-Z0-9 ]', '', example)
        example = example.replace(row['key'], '<b class="red white-text">'+row['key']+'</b>')
        update = ("UPDATE mapped_kata SET count = "+str(banyak)+", example = '"+example+"' WHERE `id` = "+str(row['id']))
        # print(update)
        # Insert article
        cursor = con2.cursor()
        cursor.execute(update)
        con2.commit()
        # print('masuk')
        cursor.close()
