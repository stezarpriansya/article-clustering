import pandas as pd
import numpy as np
import datetime
import re
import os
# set working directory first
cwd = os.getcwd()
os.chdir("D:\\OJT FILES\\article-clustering\\STEZAR")

# NLTK
import nltk
from nltk import word_tokenize
from nltk.util import ngrams

# Gensim
import gensim
import gensim.corpora as corpora
from gensim.models import CoherenceModel
from gensim.models import ldamodel

# Plotting tools
import pyLDAvis
import pyLDAvis.gensim  # don't skip this
import matplotlib.pyplot as plt
%matplotlib inline

# Enable logging for gensim - optional
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.ERROR)

import warnings
warnings.filterwarnings("ignore",category=DeprecationWarning)
#-------------------------------------------------------------------------------------------------
# TODO: Conacantenate with another attribit like source and category and then filter all news except from Seva.id
# import data
raw_data = pd.read_csv('raw_after_clean_afterstop4stem.csv')
raw_data = raw_data['content2'].apply(lambda x: nltk.word_tokenize(x))
clean_texts = raw_data['content2'].tolist()
# Create Dictionary
id2word = corpora.Dictionary(clean_texts)

# Term Document Frequency
corpus = [id2word.doc2bow(text) for text in clean_texts]

# Build LDA model
lda_model = ldamodel.LdaModel(corpus=corpus, id2word=id2word, num_topics=20, random_state=123, update_every=1, chunksize=10000, passes=10, alpha='auto', per_word_topics=True)

# Print the Keyword in the 10 topics
pprint(lda_model.print_topics())
doc_lda = lda_model[corpus]

# EVALUATING MODEL
# Compute Perplexity
print('\nPerplexity: ', lda_model.log_perplexity(corpus))  # a measure of how good the model is. lower the better.

# Compute Coherence Score
coherence_model_lda = CoherenceModel(model=lda_model, texts=data_lemmatized, dictionary=id2word, coherence='c_v')
coherence_lda = coherence_model_lda.get_coherence()
print('\nCoherence Score: ', coherence_lda)

# Visualize the topics
pyLDAvis.enable_notebook()
vis = pyLDAvis.gensim.prepare(lda_model, corpus, id2word)
vis
