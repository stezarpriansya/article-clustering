from __future__ import print_function

import logging
import os.path
import sys
import six

from gensim.corpora import WikiCorpus

program = os.path.basename(sys.argv[0])
logger = logging.getLogger(program)

logging.basicConfig(format='%(asctime)s: %(levelname)s: %(message)s')
logging.root.setLevel(level=logging.INFO)
logger.info("running %s" % ' '.join(sys.argv))

namaFileInput = "idwiki-latest-pages-articles.xml.bz2"
namaFileOutput = "wiki.id.case.text"

space = " "
i = 0

output = open(namaFileOutput, 'w')

# lower=False: huruf kecil dan besar dibedakan
wiki = WikiCorpus(namaFileInput, lemmatize=False, dictionary={}, lower=True)
for text in wiki.get_texts():
    if six.PY3:
        output.write(bytes(' '.join(text), 'latin').decode('latin') + '\n')
    #   ###another method###
    #    output.write(
    #            space.join(map(lambda x:x.decode("utf-8"), text)) + '\n')
    else:
        output.write(space.join(text) + "\n")
    # output.write(' '.join(text) + '\n')
    # i = i + 1
    # if i % 10000 == 0:
    #     logger.info("Saved " + str(i) + " articles")

output.close()
logger.info("Finished Saved " + str(i) + " articles")
