import re
from collections import Counter
import os
import pandas as pd
import itertools
import nltk
from nltk import word_tokenize
# set working directory first
cwd = os.getcwd()
os.chdir("D:\\OJT FILES\\article-clustering\\STEZAR")

def viterbi_segment(text):
    """
    Using Viterbi Algorithm: https://en.wikipedia.org/wiki/Viterbi_algorithm
    """
    probs, lasts = [1.0], [0]
    for i in range(1, len(text) + 1):
        prob_k, k = max((probs[j] * word_prob(text[j:i]), j)
                        for j in range(max(0, i - max_word_length), i))
        probs.append(prob_k)
        lasts.append(k)
    words = []
    i = len(text)
    while 0 < i:
        words.append(text[lasts[i]:i])
        i = lasts[i]
    words.reverse()
    return words, probs[-1]

def word_prob(word): return dictionary[word] / total
def words(text): return re.findall('[a-z]+', text.lower())
#COBA BUAT FUNGSI
def special_match(strg, search=re.compile(r'[^a-z0-9]').search):
    return not bool(search(strg))

def collect_mapped_dict(list_word):
    dict = set(my_dict)
    for word in list_word:
        if word in dict:
            continue
        else:
            split = viterbi_segment(word)
            lengths = [len(x) for x in split[0]]
            if 1 in lengths:
                continue
            else:
                if mapped_dict.get(word, None) == None:
                    mapped_dict[word] = ' '.join(split[0])
                    file = open("mapped_dict.txt", "w+")
                    file.write(word+','+' '.join(split[0])+'\n')
                    file.close()

def unstack_list(my_list):
    return list(itertools.chain(*my_list))

def split_word(word):
    dict = set(my_dict)
    if word in dict:
        return [word]
    else:
        split = viterbi_segment(word)
        lengths = [len(x) for x in split[0]]
        if 1 in lengths:
            return [word]
        else:
            return split[0]

# INIT
dictionary = Counter(words(open('all1.txt').read()))
my_dict = pd.read_table('dict.txt')['lemma']
max_word_length = max(map(len, dictionary))
total = float(sum(dictionary.values()))
viterbi_segment('motordi')
# COLLECT MAPPED DICT
all_data = pd.read_pickle('all_data_clean_after_stemming.pkl')

# CREATE DICT
# text_df = pd.read_pickle('raw_after_clean_aftertri.pkl')
# text = text_df.content.tolist()
# text = ' '.join(text)
# text = word_tokenize(text)
# with open('all.txt', 'w+') as f:
#     for item in text:
#         if (special_match(item)):
#             f.write("%s\n" % item)

# tes = words(open('all.txt').read())
# my_dict1 = set(my_dict)
# my_dictxx = [x for x in my_dict1 if len(x) > 1]
# tes = [x for x in tes if x in my_dict]
# merged_dict = tes + my_dictxx
# with open('all1.txt', 'w+') as f:
#     for item in merged_dict:
#         if (special_match(item)):
#             f.write("%s\n" % item)

mapped_dict = {}
for index, row in all_data.iterrows():
    collect_mapped_dict(row['content2'].split())

# 169215 kata non dict
len(mapped_dict)
with open('mapped_dict.txt', 'w+') as f:
    for key, value in mapped_dict.items():
        f.write(key+','+value+'\n')


# EXAMPLE
# all_data['content2'] = all_data['content2'].apply(lambda words: unstack_list([split_word(word) for word in words]))
