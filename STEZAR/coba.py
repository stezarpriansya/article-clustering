import json
import requests

def get_proxy1():
    url = "http://gimmeproxy.com/api/getProxy"
    s = requests.Session()
    response = s.get(url)
    if response.text == '':
        return get_proxy1()
    hasil = json.loads(response.text)
    return {hasil['protocol'] : hasil['ipPort']}

prox = get_proxy1()
prox

session = requests.Session()
session.proxies = prox
my_ip = session.get('http://jsonip.com', proxies = prox)
my_ip.text
