import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import mysql.connector
import nltk
from nltk import word_tokenize
from nltk.util import ngrams
%matplotlib inline
import datetime
from collections import Counter
import wordcloud

import os
cwd = os.getcwd()
os.chdir("D:\\OJT FILES\\article-clustering\\STEZAR")

# TRIGRAMS PRA PROCESSING
# import data
# ngrams_df = pd.read_csv('tokenized_raw.csv', index_col=0)
# ngrams_df.set_index('id', inplace=True)

# filter not null
# ngrams_df = ngrams_df[ngrams_df['content2'].notnull()]
#
# #tokenize
# ngrams_df['tokenized_text'] = ngrams_df['content2'].apply(lambda x: nltk.word_tokenize(x.lower()))
#
# # create trigrams per row
# ngrams_df['trigrams'] = ngrams_df['tokenized_text'].apply(lambda x: list(map(' '.join, ngrams(x, 3))))
#
# #convert resultset of tigrams list to string
# ngrams_df['trigrams_str'] = ngrams_df['trigrams'].apply(lambda x: ','.join(x))
#
# all_trigram = ngrams_df.trigrams_str.str.cat(sep=', ')
#
# all_trigram_token = all_trigram.split(',')
#
# freq = nltk.FreqDist(ch for ch in all_trigram_token)
#
# freq_commons = freq.most_common(50)
#
# freq_df = pd.DataFrame(freq_commons, columns=['word', 'freq'])

# from sklearn.feature_extraction.text import TfidfVectorizer
# tfidf = TfidfVectorizer(lowercase=True, analyzer='word' ,ngram_range=(3,3))
# train_vect = tfidf.fit_transform(ngrams_df['content2'])
# weights = np.asarray(train_vect.mean(axis=0)).ravel().tolist()
# weights_df = pd.DataFrame({'term': tfidf.get_feature_names(), 'weight': weights})
# weights_df.to_pickle('tfidftrigrams.pkl')
# weights_df.sort_values(by='weight', ascending=False).head(50)

weights_df = pd.read_pickle('tfidftrigrams.pkl')
weights_df = weights_df.sort_values(by='weight', ascending=False)
filter_df = weights_df[weights_df['weight'] > 0.000765]

filter_words = ['bergabung bersama kami', 'bersama kami di', 'kami di facebookdan', 'temukan mobil idaman', 'yang berada di', 'presiden joko widodo', 'mobil idaman di', 'temukan mobil dalam', 'jakarta antara news', 'liputan6 com jakarta', 'tempo co jakarta', 'grid oto com', 'di facebookdan twitter']

filter_df_wo_words = filter_df[~filter_df.term.isin(filter_words)]
filter_df_wo_words
filter_df_wo_words.to_pickle('filter_tags.pkl')
# stringtags = ' '.join(list(filter_df_wo_words.term))
# stringtags = set(stringtags.split(' '))
# stringtags

#UNIGRAM
from sklearn.feature_extraction.text import TfidfVectorizer
tfidf = TfidfVectorizer(lowercase=True, analyzer='word' ,ngram_range=(1,1))
train_vect = tfidf.fit_transform(ngrams_df['content2'])
weights = np.asarray(train_vect.mean(axis=0)).ravel().tolist()
weights_df = pd.DataFrame({'term': tfidf.get_feature_names(), 'weight': weights})
weights_df.to_pickle('tfidfunigrams.pkl')
weights_df.sort_values(by='weight', ascending=False).head(50)

filter_df_wo_words = pd.read_pickle('filter_tags.pkl')
raw_data = pd.read_csv('article_201809061411.csv')
raw_data.head()
