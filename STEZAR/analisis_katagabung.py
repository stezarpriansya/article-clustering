import pandas as pd
import numpy as np
import os
# set working directory first
cwd = os.getcwd()
os.chdir("D:\\OJT FILES\\article-clustering\\STEZAR")
from Sastrawiv2.Stemmer.StemmerFactory import StemmerFactory

data_map = pd.read_csv('mapped_kata_dump_17092018.csv', header=None)
data_map.columns = ['id','key', 'value', 'example', 'count', 'status', 'attempt']
data_map = data_map[(data_map.status == 0) & (data_map['count'] <= 92)].sort_values(by='count', ascending=False)
data_map.reset_index(inplace=True)
dict = pd.read_table('dict.txt')
dict = set(dict.lemma)
# str(data_map[data_map.example.isnull()].head()['example'][151630])
import whoosh.index as index
from whoosh.qparser import QueryParser
ix = index.open_dir("index_cleansing")
searcher = ix.searcher()
query_parser = QueryParser("content", ix.schema)
factory = StemmerFactory()
stemmer = factory.create_stemmer()
data_map.drop('index', axis=1, inplace=True)
data_map.shape
# 167039 -> 85000
# data_map[10:19]
for index, row in data_map[76141:85000].iterrows():
    # print(str(index)+" - "+str(row['id']))
    if str(row['example']).lower() == 'nan':
        example = row['example'].lower()
        bef = match_before(row['key'], example)
        aft = match_after(row['key'], example)
        if (bef != '') or (aft != ''):
            example = bef+' '+aft
        example = re.sub(r'[^a-zA-Z0-9 ]', '', example)
        example = example.replace(row['key'], '<b class="red white-text">'+row['key']+'</b>')
        data_map.loc[data_map['id'] == row['id'], 'example'] = example
    root = stemmer.stem(row['key'])
    # index
    if (root != row['key']) and (root in dict):
        data_map.loc[data_map['id'] == row['id'], 'status_kata'] = 'stemmed'
    elif (root == row['key']) and (root in dict):
        data_map.loc[data_map['id'] == row['id'], 'status_kata'] = 'root'
    elif (root != row['key']) and (root not in dict):
        data_map.loc[data_map['id'] == row['id'], 'status_kata'] = 'miss_stemmed'
    else:
        list_koreksi = row['value'].split()
        for kor in list_koreksi:
            if len(kor) <= 1:
                data_map.loc[data_map['id'] == row['id'], 'status_kata'] = 'uknown'
                break
            query = query_parser.parse(kor)
            results = searcher.search(query, limit=1)
            banyak = results.estimated_length()
            if (banyak >= 20) and (kor in dict):
                # sering muncul dan ada di dict
                data_map.loc[data_map['id'] == row['id'], 'status_kata'] = 'merged'
                if len(kor) == 2:
                    # cuman dua karakter
                    data_map.loc[data_map['id'] == row['id'], 'status_kata'] = 'warning'
            else:
                data_map.loc[data_map['id'] == row['id'], 'status_kata'] = 'uknown'
                break

    if not os.path.isfile('analisis_kata_gabung.csv'):
       data_map.iloc[index:index+1].to_csv('analisis_kata_gabung.csv')
    else: # else it exists so append without writing the header
       data_map.iloc[index:index+1].to_csv('analisis_kata_gabung.csv', mode='a', header=None)
    file = open("analisis_kata.log", "w")
    file.write('memasukkan data ke-'+str(index)+" - "+str(row['id']))
    file.close()

#------------- AFTER LABELLING ------------------#
import os
import requests
import json
import time

data_map = pd.read_csv('analisis_kata_gabung.csv', index_col = 0)
unknown1 = data_map[(data_map.status_kata == 'uknown') & (data_map['count'] == 1)]
warning1 = data_map[(data_map.status_kata == 'warning') & (data_map['count'] == 1)]
merged = data_map[(data_map.status_kata == 'merged')]
mregedstr = merged[(merged['key'].str.len() >= 8) | (data_map['count'] == 1)]
merged_gabung = pd.concat([unknown1, warning1, mregedstr])
# merged_gabung.tail()
merged_gabung['clean_example'] = merged_gabung['example'].apply(lambda x: x.replace("<b class='red darken-1 white-text'>", ""))
merged_gabung['clean_example'] = merged_gabung['clean_example'].apply(lambda x: x.replace("</b>", ""))
#------------ DEF TRANSLATE -------------#
dicts = []
def translate(word):
    url = "http://google-translate-scrap.herokuapp.com/translate?word="+word
    response = requests.get(url)
    hasil = json.loads(response.text)
    if hasil['correctedText'] == '':
        return word
    return hasil['correctedText']

# merged_gabung['value2'] = merged_gabung['key'].apply(lambda x : translate(x))
# merged_gabung[merged_gabung['key'] == 'memperlihatkahteaser']
merged_gabung.reset_index(inplace=True)
merged_gabung.drop('index', axis=1, inplace=True)

def get_proxy1():
    url = "http://gimmeproxy.com/api/getProxy"
    s = requests.Session()
    response = s.get(url)
    if response.text == '':
        return get_proxy1()
    hasil = json.loads(response.text)
    return {hasil['protocol'] : hasil['ipPort']}

def get_proxy2():
    url = "https://api.getproxylist.com/proxy"
    s = requests.Session()
    response = s.get(url)
    if response.text == '':
        return get_proxy2()
    hasil = json.loads(response.text)
    return {hasil['protocol'] : hasil['ip']+":"+str(hasil['port'])}

def translate_py(proxy, word):
    from googletransv2.googletransste import Translator
    translator = Translator(proxies=proxy)
    translation = translator.translate(word, dest='en', src='id')
    res = translation.extra_data['possible-mistakes']
    if res == None:
        return None
    return res[1]

# import ipgetter
# print(ipgetter.myip())
i = 0
proxy = get_proxy1()
proxy
# from googletrans12 import Translator
# translator = Translator(proxies=get_proxy())
# translations = translator.translate('aku adalah anak gembala' , dest='en', src='id')

# merged_gabung[3854:].head()
# 3854

import time
for index, row in merged_gabung[55593:].iterrows():
    # print(row['key'])
    list = []
    temp = {}
    temp['id'] = row['id']
    try:
        temp['trans1'] = translate_py(proxy, row['key'])
    except:
        proxy = get_proxy1()
        temp['trans1'] = translate_py(proxy, row['key'])

    list.append(temp)
    df = pd.DataFrame(list)
    if not os.path.isfile('dicts_trans.csv'):
       df.iloc[0:].to_csv('dicts_trans.csv')
    else: # else it exists so append without writing the header
       df.iloc[0:].to_csv('dicts_trans.csv', mode='a', header=None)
    file = open("analisis_katagabung_gtrans.log", "w")
    file.write('memasukkan data ke-'+str(index)+' -> '+row['key'])
    file.close()
    i = i+1
    time.sleep(5)
    if i >= 1000:
        time.sleep(60)
        i = 0
