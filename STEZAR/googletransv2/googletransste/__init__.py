"""Free Google Translate API for Python. Translates totally free of charge."""
__all__ = 'Translator',
__version__ = '2.3.0'


from googletransv2.googletransste.client import Translator
from googletransv2.googletransste.constants import LANGCODES, LANGUAGES
