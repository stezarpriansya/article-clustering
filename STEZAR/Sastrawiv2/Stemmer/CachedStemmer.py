#from Sastrawi.Stemmer.StemmerInterface import StemmerInterface
from Sastrawi.Stemmer.Filter import TextNormalizer
import pandas as pd

class CachedStemmer(object):
    """description of class"""
    def __init__(self, cache, delegatedStemmer):
        self.cache = cache
        self.delegatedStemmer = delegatedStemmer

    def lookup_dict(word):
        check = word;
        # check2 = ''
        # query_string = ''

        dict = pd.read_table('dict.txt')
        dict = set(dict.lemma)

        if len(word) < 3:
            return False

        if re.search(r'^([a-z]+)-([a-z]+)$', word):
            if match[1] == match[2]:
                # check = match[1]
                # check2 = word
                return False

        # if check2 != "":
        #     query_string += " OR lemma LIKE '"+check2+"'";
        #
        # query = "SELECT * FROM dictionary WHERE lemma LIKE "+query_string+" LIMIT 1 "
        # cursor.execute(query)
        # result = cursor.fetchone()
        # cursor.close()
        # con.close()

        if word in dict:
            return word
        else:
            return False

    def stem(self, text):
        normalizedText = TextNormalizer.normalize_text(text)

        words = normalizedText.split(' ')
        stems = []

        for word in words:
            if self.cache.has(word):
                stems.append(self.cache.get(word))
            else:
                if lookup_dict(word):
                    stem = word
                else:
                    stem = self.delegatedStemmer.stem(word)
                self.cache.set(word, stem)
                stems.append(stem)

        return ' '.join(stems)

    def get_cache(self):
        return self.cache
