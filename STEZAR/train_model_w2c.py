import pandas as pd
import numpy as np
import datetime
import re
import os
# set working directory first
cwd = os.getcwd()
os.chdir("D:\\OJT FILES\\article-clustering\\STEZAR")

# NLTK
import nltk
from nltk import word_tokenize
from nltk.util import ngrams

# Gensim
import gensim
import gensim.corpora as corpora
from gensim.models import Word2Vec

# Visualize
import matplotlib.pyplot as plt
%matplotlib inline
from sklearn.decomposition import PCA

# import logging
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

#-------------------------------------------------------------------------------------------------
# import data
def translate_category(x):
	"""
	Function for otomotif and property category mapping
	"""
	x[0] = str(x[0])
	x[1] = str(x[1])
	x[2] = str(x[2])

	if ('seva' in x[2].lower()) and ('properti' not in x[0].lower()):
		return 'otomotif'
	elif x[0].lower() in ["oto", "otomotif", "mobil", "modifikasi"]:
		return 'otomotif'
	elif (x[0].lower() in ["autotekno"]) and (x[1].lower() in ['mobil', 'motor', 'advertorial autotekno', 'komunitas', 'modifikasi', 'rest area', 'test', 'tips & trik']):
		return 'otomotif'
	elif (x[0].lower() in ["home"]) and (x[1].lower() in ['otomotif']):
		return 'otomotif'
	elif (x[0].lower() in ["home"]) and (x[1].lower() in ['properti']):
		return 'properti'
	elif (x[0].lower() in ['bisnis', 'properti', 'finance', 'ekbis', 'ekonomi', 'economy']) and ('properti' in x[1].lower()):
		return 'properti'
	else:
		return x[0].lower()
raw_data = pd.read_pickle('raw_after_clean_afterstop_w2v_2_cleansing3_FIXX.pkl') #raw data untuk w2v
oto_len = raw_data[raw_data.clean_category == 'otomotif'].shape[0]
pro_len = raw_data[raw_data.clean_category == 'properti'].shape[0]
oto_len + pro_len
oto_len
pro_len

raw_data.to_pickle('raw_after_clean_afterstop_w2v_2_cleansing4_FIXX.pkl')
# raw_data[raw_data.clean_category == 'otomotif']['word']
raw_data.drop(raw_data[raw_data['id'] == 119970].index, inplace=True)
raw_data.drop(raw_data[raw_data['id'] == 169337].index, inplace=True)
raw_data.drop(raw_data[raw_data['id'] == 171108].index, inplace=True)
raw_data.drop(raw_data[raw_data['id'] == 236771].index, inplace=True)
raw_data.drop(raw_data[raw_data['id'] == 342711].index, inplace=True)
raw_data['word_count'] = raw_data['content'].apply(lambda x: len(str(x).split(" ")))
raw_data['clean_category'] = raw_data[['category', 'subcategory', 'source']].apply(translate_category, axis=1)
train = raw_data[raw_data.source != 'seva']
# 92232992 wiki words
# 347740 documents wiki
sum(train.word_count)
train.shape
test = raw_data[raw_data.source == 'seva']
test.shape
sum(test.word_count)
test1 = test[(test.subcategory != 'berita-terbaru') & (test.subcategory != 'berita-otomotif')]
test1.shape
sum(test1.word_count)
%time train['content'] = train['content'].apply(lambda x: nltk.word_tokenize(x))
train.head()
clean_texts = train['content'].tolist()
clean_texts_ready = [[y for y in nltk.word_tokenize(x)] for x in clean_texts]
train['content2'] = clean_texts_ready
#---------CHECKPOINT 1------------------------------
train[['id', 'content']].to_pickle('siap_w2v.pkl')

train = pd.read_pickle('siap_w2v.pkl')
train = train.content.tolist()

model = gensim.models.Word2Vec(train, size=400, window=5, min_count=5, workers=10)
model.save("word2vec1.model")
# model.train(train, total_examples=len(train), epochs=10)

#---------LOAD MODEL----------
model = gensim.models.Word2Vec.load('word2vec1.model')
# Find related word through w2c
word_to_find = 'modif'
model.wv.most_similar(positive=word_to_find)

# similarity between two different words
model.wv.similarity(w1="denso", w2="ngk")

# Visualize Word Embedding
# Plot Word Vectors Using PCA
X = model[model.wv.vocab]

pca = PCA(n_components=2)
result = pca.fit_transform(X)

plt.scatter(result[:, 0], result[:, 1])
words = list(model.wv.vocab)
for i, word in enumerate(words):
	plt.annotate(word, xy=(result[i, 0], result[i, 1]))

plt.show()


#---------LOAD MODEL---------- SG Wiki
model = gensim.models.Word2Vec.load('model_w2v_wiki_id')
model.train(train, total_examples=model.corpus_count, epochs=model.epochs)
model.save('model_w2v_wiki_berita')
