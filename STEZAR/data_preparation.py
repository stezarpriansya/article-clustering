import pandas as pd
import numpy as np
import os
# set working directory first
cwd = os.getcwd()
os.chdir("D:\\OJT FILES\\article-clustering\\STEZAR")

from Sastrawiv2.Stemmer.StemmerFactory import StemmerFactory
import nltk
from nltk import word_tokenize
from nltk.util import ngrams
import datetime
import re
# from IPython.display import clear_output

# def lookup_dict(word):
#     check = word;
#     check2 = ''
#     query_string = ''
#
#     con = mysql.connector.connect(user='root', password='', host='127.0.0.1', database='lemmatizer')
#     cursor = con.cursor()
#
#     if len(word) < 3:
#         return False
#
#     if re.search(r'^([a-z]+)-([a-z]+)$', word):
#         if match[1] == match[2]:
#             check = match[1]
#             check2 = word
#
#     query_string = "'"+check+"'";
#
#     if check2 != "":
#         query_string += " OR lemma LIKE '"+check2+"'";
#
#     query = "SELECT * FROM dictionary WHERE lemma LIKE "+query_string+" LIMIT 1 "
#     cursor.execute(query)
#     result = cursor.fetchone()
#     cursor.close()
#     con.close()
#
#     if(result[0]['lemma'] == word):
#         return word
#     else:
#         return False

# def stemming(stemmer, content):
#     # stem
#     output = stemmer.stem(content)
#     return output
# function to create category otomotif and property
def translate_category(x):
    """
    Function for otomotif and property category mapping
    """
    x[0] = str(x[0])
    x[1] = str(x[1])
    x[2] = str(x[2])

    if ('seva' in x[2].lower()) and ('properti' not in x[0].lower()):
        return 'otomotif'
    elif x[0].lower() in ["oto", "otomotif"]:
        return 'otomotif'
    elif (x[0].lower() in ["autotekno"]) and (x[1].lower() in ['mobil', 'motor']):
        return 'otomotif'
    elif (x[0].lower() in ['Bisnis', 'Properti', 'finance', 'Ekbis', 'Home']) and ('properti' in x[1].lower()):
        return 'properti'
    else:
        return x[0].lower()

# function average length of word per sentence
def avg_word(sentence):
  words = sentence.split()
  return (sum(len(word) for word in words)/len(words))

# function take 2 after word
def match_after(word, text):
    return re.search(r'('+word+'\s+((?:\w+(?:\s+|$)){2}))', text).group(0) if re.search(r'('+word+'\s+((?:\w+(?:\s+|$)){2}))', text) else ''

# function take 2 before word
def match_before(word, text):
    return re.search(r'((?:\S+\s+){0,2}\b'+word+')', text).group(0) if re.search(r'((?:\S+\s+){0,2}\b'+word+')', text) else ''

# TODO: REMOVE STOPWORDS
def remove_stopwords(content_clean):
    # CLEANSING STOPWORDS FROM TALA DICT
    tala = pd.read_table("tala_stopwords.txt")
    stopwords_tala = set(tala['tala_stopword'].tolist())
    list_content_clean = ' '.join([x.lower() for x in content_clean.split() if x.lower() not in stopwords_tala])

    return list_content_clean

# REMOVE WEBSITE URL ON CONTENT
def remove_source_url(content_clean):
    # CLEANSING WEBSITE URL FROM CONTENT
    url = pd.read_table("website_stopwords.txt")
    stopwords_url = set(url['website_stopword'].tolist())
    content_clean = ' '.join([x for x in content_clean.split() if x not in stopwords_url])

    return content_clean

# REMOVE WEBSITE URL ON CONTENT
def remove_source_url2(content_clean):
    # CLEANSING WEBSITE URL FROM CONTENT
    url = pd.read_table("website_stopwords_nonsym.txt")
    stopwords_url = set(url['website_stopword'].tolist())
    for x in stopwords_url:
        content_clean = content_clean.replace(x, '')

    return content_clean

# TODO: REMOVE TRIGRAMS
def remove_trigrams(content_clean):
    # Cleansing content from css/html/js tags
    # import list of tags
    filter_tags = pd.read_pickle('filter_tags.pkl')
    tags = ' '.join(list(filter_tags.term))
    # filter only unique web tags
    unigram_tags = set(tags.split(' '))

    #trigrams tags
    trigram_tags = set(filter_tags.term)

    # execute clean web tags on property
    content_clean = " ".join(x for x in content_clean.split() if (x not in unigram_tags) and ((match_after(x, content_clean) not in trigram_tags) or (match_before(x, content_clean) not in trigram_tags)))

    return content_clean

# TODO: REMOVE UNIGRAMS
def remove_unigrams(content_clean):
    # TODO: CLEANSING with unigrams
    # clean common unigrams
    common_unigrams = pd.read_pickle('common_unigrams.pkl')
    list_common = set(common_unigrams.term)
    content_clean = " ".join(x for x in content_clean.split() if x not in list_common)

    # clean rare unigrams
    rare_unigrams = pd.read_pickle('rare_unigrams.pkl')
    list_rare = set(rare_unigrams.term)
    content_clean = " ".join(x for x in content_clean.split() if x not in list_rare)

    return content_clean

# CLEANSING DATA
def cleansing_content(row):
    """
    This function use for clean news content like html/css/js tags, punctuation, split camelcase, and many more
    """
    # define variable
    source = row[0]
    category = row[1]
    content_clean = row[2]

    # define regex for tokenize a words
    pattern = r'\w+' #for any word char
    pattern2 = r'.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)' #tokenize for camelcase

    # REMOVE WEBSITE URL FIRST
    content_clean = remove_source_url(content_clean)

    # cleansing with regex
    if 'carmudi' in source.lower():
        # CARMUDI
        # mendeteksi nama penulis di akhir kalimat (abc)
        # (\([a-zA-Z]+\)$)
        carmudi_pattern1 = r'(\([a-zA-Z]+\)$)'
        content_clean = re.sub(carmudi_pattern1, ' \n ', content_clean)
        # mendeteksi nama penulis di akhir kalimat (abc/abc)
        # (\([a-zA-Z]+\/[a-zA-Z]+\)$)
        carmudi_pattern2 = r'(\([a-zA-Z]+\/[a-zA-Z]+\)$)'
        content_clean = re.sub(carmudi_pattern2, ' \n', content_clean)

    if 'cintamobil' in source.lower():
        # CINTAMOBIL
        # mendeteksi dan menghapus link sisip (>>>...)
        content_clean = ' \n '.join([re.sub(r'(>>>.+)', ' ', x) for x in nltk.regexp_tokenize(content_clean, pattern2)])

    if 'detik' in source.lower():
        # DETIK
        # mendeteksi (abc/abc)
        # (\([a-zA-Z]+\/[a-zA-Z]+\)$)
        detik_pattern = r'(\([a-zA-Z]+\/[a-zA-Z]+\)$)'
        content_clean = re.sub(detik_pattern, ' \n ', content_clean)

    if 'kompas' in source.lower():
        # KOMPAS
        # mendeteksi baca juga
        kompas_pattern = r'(baca juga:.*?(\.|$))'
        content_clean = re.sub(kompas_pattern, ' \n ', content_clean)

    if 'metrotvnews' in source.lower():
        # METROTVNEWS
        # mendeteksi nama penulis di akhir paragraf (abc)
        metro_pattern = r'(\([a-zA-Z]+\)$)'
        content_clean = re.sub(metro_pattern, ' \n ', content_clean)

    if 'mobil123' in source.lower():
        # MOBIL123
        # mendeteksi nama penulis di akhir paragraf [abc/abc]
        mobil123_pattern1 = r'(\[([a-zA-Z])+\/[a-zA-Z]+\]$)'
        content_clean = re.sub(mobil123_pattern1, ' \n ', content_clean)

        # mendeteksi nama penulis di akhir paragraf [dol]
        mobil123_pattern2 = r'(\[([a-zA-Z])*\]$)'
        content_clean = re.sub(mobil123_pattern2, ' \n ', content_clean)

    if 'okezone' in source.lower():
        # OKEZONE
        # mendeteksi nama penulis di akhir paragraf (abc)
        okezone_pattern = r'(\([a-zA-Z]+\)$)'
        content_clean = re.sub(okezone_pattern, ' \n ', content_clean)

    if 'oto' in source.lower():
        # OTO
        # mendeteksi (dol) di akhir paragraf
        oto_pattern1 = r'(\([a-zA-Z]+\)$)'

        content_clean = re.sub(oto_pattern1, ' \n ', content_clean)
        # mendeteksi (abc/abc)
        oto_pattern2 = r'(\([a-z]+\/[a-z]+\)$)'
        content_clean = re.sub(oto_pattern2, ' \n ', content_clean)

        # mendeteksi [dol/dol] di akhir paragraf
        oto_pattern3 = r'(\[([a-zA-Z])+\/[a-zA-Z]+\]$)'
        content_clean = re.sub(oto_pattern3, ' \n ', content_clean)

        # mendeteksi [dol] di akhir paragraf
        oto_pattern4 = r'(\[([a-zA-Z])*\]$)'
        content_clean = re.sub(oto_pattern4, ' \n ', content_clean)

    if 'otosia' in source.lower():
        # OTOSIA
        # mendeteksi (dol) di akhir paragraf
        otosia_pattern1 = r'(\([a-zA-Z]+\)$)'
        content_clean = re.sub(otosia_pattern1, ' \n ', content_clean)

        # mendeteksi (abc/abc)
        otosia_pattern2 = r'(\([a-z]+\/[a-z]+\)$)'
        content_clean = re.sub(otosia_pattern2, ' \n ', content_clean)

        # mendeteksi (abc/abc/abc)
        otosia_pattern3 = r'(\([a-zA-Z]+\/[a-zA-Z]+\/[a-zA-Z]+\)$)'
        content_clean = re.sub(otosia_pattern3, ' \n ', content_clean)

        # mendeteksi (abc/abc/abc/abc)
        otosia_pattern4 = r'(\([a-zA-Z]+\/[a-zA-Z]+\/[a-zA-Z]+\/[a-zA-Z]+\)$)'
        content_clean = re.sub(otosia_pattern4, ' \n ', content_clean)

    if 'propertiterkini' in source.lower():
        # PROPERTITERKINI
        # mendeteksi penulis dan baca juga
        properti_pattern = r'(\[[a-zA-Z].+\/[a-zA-Z].+\]+Baca Juga.+$)'
        content_clean = re.sub(properti_pattern, ' \n ', content_clean)

    if 'rajamobil' in source.lower():
        # RAJAMOBIL
        # mendeteksi comment in script
        rajamobil_pattern1 = r'(&lt;span data-mce-type=\"bookmark\".+$)'
        content_clean = re.sub(rajamobil_pattern1, ' \n ', content_clean)

        # mendeteksi [dol] di akhir paragraf
        rajamobil_pattern2 = r'(\[([a-zA-Z])*\]$)'
        content_clean = re.sub(rajamobil_pattern2, ' \n ', content_clean)

        # mendeteksi [dol] di akhir paragraf + iklan
        rajamobil_pattern3 = r'(\[([a-zA-Z])*\].+$)'
        content_clean = re.sub(rajamobil_pattern3, ' \n ', content_clean)

    if 'rumahku' in source.lower():
        # RUMAHKU
        rumahku_pattern = r'(Untuk keperluan informasi dapat mengunjungi website:http://www.primesystem.id.+$)'
        content_clean = re.sub(rumahku_pattern, ' \n ', content_clean)

    if 'sindonews' in source.lower():
        # SINDONEWS
        # regex
        sindo_pattern = r'(\([a-zA-Z]+\)$)'
        content_clean = re.sub(sindo_pattern, ' \n ', content_clean)

    # remove punctuation and tokenize only words
    list_content_clean = nltk.regexp_tokenize(content_clean, pattern)

    # split camelcase word
    content_clean = ' '.join([' '.join(nltk.regexp_tokenize(word, pattern2)) for word in list_content_clean])

    return content_clean

# ------------------MAIN CLASS---------------------
# init connection with mysql host
# con = mysql.connector.connect(user='root', password='', host='127.0.0.1', database='news_db')

# get all raw data
# raw_data = pd.read_sql('SELECT * FROM article', con)
raw_data = pd.read_csv('article_201809061411.csv')

raw_data = raw_data[(raw_data.content.notnull()) & (raw_data.category.notnull())]
# raw_data.columns
# handling otomotif and property category
raw_data['clean_category'] = raw_data[['category', 'subcategory', 'source']].apply(translate_category, axis=1)

# check the result from category mapping
raw_data[raw_data['clean_category'].isnull()]['category'].unique()
raw_data[raw_data['clean_category'].isnull()]['subcategory'].unique()
raw_data['clean_category'].unique()


# create derivative variable
# word count
raw_data['word_count'] = raw_data['content'].apply(lambda x: len(str(x).split(" ")))
#average word length
raw_data['avg_word'] = raw_data['content'].apply(lambda x: avg_word(x))
raw_data = raw_data[raw_data.word_count >= 10]
# raw_data.head()
# Cleansing content
raw_data['content'] = raw_data[['source', 'clean_category', 'content']].apply(cleansing_content, axis=1)
#-------------- Read Data After Cleansing--------------------

# remove common trigrams in property category article
raw_data.loc[raw_data.clean_category=='properti', 'content'] = raw_data.loc[raw_data.clean_category=='properti', 'content'].apply(lambda x : remove_trigrams(x))
raw_data = pd.read_pickle('raw_after_clean_aftertri.pkl')
# For concate with df after stemming
# raw_data = raw_data.loc[:, ['id', 'pubdate', 'clean_category', 'source', 'word_count', 'avg_word']]
# raw_data.head()

raw_data = ''
# remove common unigrams
# raw_data['content'] = raw_data['content'].apply(lambda x : remove_unigrams(x))

# tokenize
raw_data['tokenized'] = raw_data['content'].apply(lambda x : nltk.word_tokenize(x))

# remove stopwords
raw_data['content2'] = raw_data['tokenized'].apply(lambda x : remove_stopwords(x))

# stemming
# raw_data['content2'] = raw_data['content2'].apply(lambda x : stemming(x))

# Checkpoint 02
raw_data.to_pickle('raw_after_clean_afterstop.pkl')

#-------------- Read Data After remove stopwords ----------------
raw_data2 = pd.read_pickle('raw_after_clean_afterstop.pkl')

raw_data3 = raw_data2[['id', 'content2']]

raw_data3.to_pickle('raw_after_clean_afterstop2.pkl')
#-----------------------------------------------
raw_data = pd.read_pickle('raw_after_clean_afterstop2.pkl')
raw_data['content2'] = raw_data['content2'].apply(lambda x : ' '.join(x))
raw_data['content2'] = raw_data['content2'].apply(lambda x : remove_stopwords(x))
raw_data.to_pickle('raw_after_clean_afterstop3.pkl')
raw_data = pd.read_pickle('raw_after_clean_afterstop3.pkl')
raw_data.head()
#stopwords again
raw_data['content2'] = raw_data['content2'].apply(lambda x : remove_stopwords(x))
raw_data.to_pickle('raw_after_clean_afterstop4.pkl')
#----
raw_data = pd.read_pickle('raw_after_clean_afterstop4.pkl')
# raw_data.head()
raw_data.reset_index(inplace=True)
raw_data.shape
# raw_data.iloc[350413]
# raw_data.shape
len(raw_data[170000:])
# raw_data['content2'] = raw_data['content2'].apply(lambda x : stemming(x))
factory = StemmerFactory()
stemmer = factory.create_stemmer()
raw_data.reset_index(inplace=True)
raw_data = raw_data.loc[:, ['id']]
raw_data.head()
# get latest stemmed row
# args = open('data_prep.log', 'r').readlines()
# start_row = int(args[0].replace('memasukkan data ke-', '').strip(' \n\r\t'))+1
# start_row
for index, row in raw_data[0:170000].iterrows():
    # row[0] = stemmer.stem(row[0])
    if not os.path.isfile('id_after_clean_afterstop4stem.csv'):
       raw_data.iloc[index:index+1].to_csv('id_after_clean_afterstop4stem.csv')
    else: # else it exists so append without writing the header
       raw_data.iloc[index:index+1].to_csv('id_after_clean_afterstop4stem.csv', mode='a', header=None)
    # print('memasukkan data ke-'+str(index)+' dari 170000')
    # file = open("data_prep.log", "w")
    # file.write('memasukkan data ke-'+str(index))
    # file.close()
    # clear_output()
#--------- After Stemming ---------------------
#DATA STEZAR
data_clean = pd.read_csv('raw_after_clean_afterstop4stem.csv')
id_data_clean = pd.read_csv('id_after_clean_afterstop4stem.csv')
id_data_clean.columns = ['index', 'id']
id_data_clean.shape
data_clean.columns =  ['index', 'content2']
data_clean.shape
#concate with id
data_clean = pd.merge(id_data_clean, data_clean, on='index', how='inner')
data_clean.drop('index', axis=1, inplace=True)
#DATA NELLA
data_nella = pd.read_csv('drop_duplicate_afterstop4stem1.csv')
id_data_clean = pd.read_csv('id_after_clean_afterstop4stem1.csv')
data_nella.drop('Unnamed: 0', axis=1, inplace=True)
id_data_clean.columns = ['index', 'id']
id_data_clean.shape
data_nella.columns =  ['index', 'content2']
data_nella.shape
#concate with id
data_nella = pd.merge(id_data_clean, data_nella, on='index', how='inner')
data_nella.shape
data_nella.drop('index', axis=1, inplace=True)

#----concate columns with raw_data
data_nella1 = pd.merge(data_nella, raw_data, on='id', how='inner')
data_nella1.shape
data_clean1 = pd.merge(data_clean, raw_data, on='id', how='inner')
data_clean.shape
all_data = pd.concat([data_clean1, data_nella1], axis=0)
all_data.shape
all_data.reset_index(inplace=True)
all_data.drop('index', axis=1, inplace=True)
all_data.shape
all_data.to_pickle('clean_data_afterallstemming.pkl')
#--------------- Read Clean Data---------------------
all_data = pd.read_pickle('clean_data_afterallstemming.pkl')
#350.414 Rows and 7 columns
all_data.shape
all_data_tes = all_data[all_data.id.isin(list(set(all_data.id.tolist()) - set(raw_data.id.tolist())))]
all_tes = all_data.drop_duplicates(subset=['content2'])
# after drop duplicated content, then becomes 350.075 rows
all_data.shape
all_data.to_pickle('clean_data_afterallstemming_dropdupl.pkl')
#CHECKPOINT
all_data = pd.read_pickle('clean_data_afterallstemming_dropdupl.pkl')
all_data.columns
# delete url again
all_data['content2'] = all_data['content2'].apply(lambda x: remove_source_url2(x))
all_data.head()
# word count
all_data['word_count_after_prep'] = all_data['content2'].apply(lambda x: len(str(x).split(" ")))
#average word length
all_data['avg_word_after_prep'] = all_data['content2'].apply(lambda x: avg_word(x))

# ready to LDA Modelling
all_data.to_pickle('all_data_clean_after_stemming.pkl')

# Data Prep untuk W2C
raw_data = pd.read_pickle('raw_after_clean_afterstop_w2v.pkl')
raw_data['content'] = raw_data['content'].apply(lambda x: remove_source_url2(x.lower()))
raw_data.to_pickle('raw_after_clean_afterstop_w2v_2.pkl')

# Data Prep again
raw_data = pd.read_pickle('raw_after_clean_afterstop_w2v_2.pkl')
filter_tags = pd.read_pickle('filter_tags.pkl')
for tags in set(filter_tags.term):
    raw_data['content'] = raw_data['content'].apply(lambda x : x.replace(tags, ''))
filter_tags
raw_data['content'] = raw_data['content'].apply(lambda x : x.lower().replace('asi games', 'asian games'))

raw_data['content'] = raw_data['content'].apply(lambda x : x.lower().replace('a a p', ''))

raw_data['content'] = raw_data['content'].apply(lambda x : x.lower().replace('mobil idam', 'mobil idaman'))

raw_data['content'] = raw_data['content'].apply(lambda x : x.lower().replace('display div gpt ad', ''))
raw_data['content'] = raw_data['content'].apply(lambda x : x.lower().replace('facebookdan', 'facebook dan'))

raw_data.to_pickle('raw_after_clean_afterstop_w2v_2_cleansing2.pkl')

#AGAIN
raw_data = pd.read_pickle('raw_after_clean_afterstop_w2v_2_cleansing2.pkl')
raw_data['content'] = raw_data['content'].apply(lambda x: x.replace('googletag googletag display', ''))
raw_data['content'] = raw_data['content'].apply(lambda x: x.replace('googletag', ''))
raw_data['content'] = raw_data['content'].apply(lambda x: x.replace('googletag googletag', ''))
raw_data['content'] = raw_data['content'].apply(lambda x: x.replace('googletag display', ''))
raw_data['content'] = raw_data['content'].apply(lambda x: x.replace('large topright', ''))
raw_data['content'] = raw_data['content'].apply(lambda x: x.replace('inter mil', 'inter milan'))


raw_data.to_pickle('raw_after_clean_afterstop_w2v_2_cleansing3.pkl')
raw_data = pd.read_pickle('raw_after_clean_afterstop_w2v_2_cleansing3.pkl')
raw_data['word_count'] = raw_data['content'].apply(lambda x: len(str(x).split(" ")))
raw_data = raw_data[raw_data.word_count >= 10]
raw_data.head()
raw_data.drop_duplicates(subset=['content'], inplace=True)
raw_data.drop(raw_data[raw_data.id == 49616].index, inplace=True)
raw_data = raw_data[(raw_data.content.notnull()) & (raw_data.category.notnull())]
raw_data.shape
ngrams_df = pd.read_pickle('CLEAN_DATA_STEM_BISMILLAH_081018.pkl')
set_df = set(ngrams_df.id.tolist())
len(set_df)
set_raw = set(raw_data.id.tolist())
len(set_raw)
selisih = set_raw - set_df
selisih
raw_data_cek = raw_data[~raw_data.id.isin(list(selisih))]
raw_data_cek.shape
set_df = set(ngrams_df.id.tolist())
len(set_df)
set_raw1 = set(raw_data_cek.id.tolist())
len(set_raw1)
selisih1 = set_df - set_raw1
selisih1
raw_data[raw_data.id.isin(list(selisih1))].iloc[0]['content']
ngrams_df = ngrams_df[~ngrams_df.id.isin(list(selisih1))]
ngrams_df.shape
raw_data_cek['word_count'] = raw_data_cek['content'].apply(lambda x: len(str(x).split(" ")))
raw_data_cek.to_pickle('raw_after_clean_afterstop_w2v_2_cleansing3_FIXX.pkl')
ngrams_df.to_pickle('CLEAN_DATA_STEM_BISMILLAH_081018.pkl')
set(all_data_tes.id.tolist()).intersection(set(raw_data_cek.id.tolist()))
all_data[all_data.id == 27181]
all_tes[all_tes.id == 27181]
