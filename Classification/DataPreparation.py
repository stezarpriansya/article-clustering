# import os
# # set working directory first
# cwd = os.getcwd()
# os.chdir("D:\\OJT FILES\\article-clustering\\Classification")
import pandas as pd
import numpy as np
from lib.Sastrawiv2.Stemmer.StemmerFactory import StemmerFactory
import nltk
from nltk import word_tokenize
from nltk.util import ngrams
import datetime
import re

class DataPreparation:
    def translate_category(self, x):
        """
        Function for otomotif and property category mapping
        """
        x[0] = str(x[0])
        x[1] = str(x[1])
        x[2] = str(x[2])

        if ('seva' in x[2].lower()) and ('properti' not in x[0].lower()):
            return 'otomotif'
        elif x[0].lower() in ["oto", "otomotif", "mobil", "modifikasi"]:
            return 'otomotif'
        elif (x[0].lower() in ["autotekno"]) and (x[1].lower() in ['mobil', 'motor', 'advertorial autotekno', 'komunitas', 'modifikasi', 'rest area', 'test', 'tips & trik']):
            return 'otomotif'
        elif (x[0].lower() in ["home"]) and (x[1].lower() in ['otomotif']):
            return 'otomotif'
        elif (x[0].lower() in ["home"]) and (x[1].lower() in ['properti']):
            return 'properti'
        elif (x[0].lower() in ['bisnis', 'properti', 'finance', 'ekbis', 'ekonomi', 'economy']) and ('properti' in x[1].lower()):
            return 'properti'
        else:
            return x[0].lower()

    # function average length of word per sentence
    def avg_word(self, sentence):
      words = sentence.split()
      return (sum(len(word) for word in words)/len(words))

    # function take 2 after word
    def match_after(self, word, text):
        return re.search(r'('+word+'\s+((?:\w+(?:\s+|$)){2}))', text).group(0) if re.search(r'('+word+'\s+((?:\w+(?:\s+|$)){2}))', text) else ''

    # function take 2 before word
    def match_before(self, word, text):
        return re.search(r'((?:\S+\s+){0,2}\b'+word+')', text).group(0) if re.search(r'((?:\S+\s+){0,2}\b'+word+')', text) else ''

    # TODO: REMOVE STOPWORDS
    def remove_stopwords(self, content_clean):
        # CLEANSING STOPWORDS FROM TALA DICT
        tala = pd.read_table("dict/tala_stopwords.txt")
        stopwords_tala = set(tala['tala_stopword'].tolist())
        list_content_clean = [x.lower() for x in content_clean if x.lower() not in stopwords_tala]

        return list_content_clean

    # REMOVE WEBSITE URL ON CONTENT
    def remove_source_url(self, content_clean):
        # CLEANSING WEBSITE URL FROM CONTENT
        url = pd.read_table("dict/website_stopwords.txt")
        stopwords_url = set(url['website_stopword'].tolist())
        content_clean = ' '.join([x for x in content_clean.split() if x not in stopwords_url])

        return content_clean

    # REMOVE WEBSITE URL ON CONTENT
    def remove_source_url2(self, content_clean):
        # CLEANSING WEBSITE URL FROM CONTENT
        url = pd.read_table("dict/website_stopwords_nonsym.txt")
        stopwords_url = set(url['website_stopword'].tolist())
        for x in stopwords_url:
            content_clean = content_clean.replace(x, '')

        return content_clean

    # TODO: REMOVE TRIGRAMS
    def remove_trigrams(self, content_clean):
        # Cleansing content from css/html/js tags
        # import list of tags
        filter_tags = pd.read_csv('dict/filter_tags.txt')
        tags = ' '.join(list(filter_tags.term))
        # filter only unique web tags
        unigram_tags = set(tags.split(' '))

        #trigrams tags
        trigram_tags = set(filter_tags.term)

        # execute clean web tags on property
        content_clean = " ".join(x for x in content_clean.split() if (x not in unigram_tags) and ((self.match_after(x, content_clean) not in trigram_tags) or (self.match_before(x, content_clean) not in trigram_tags)))

        return content_clean

    # CLEANSING DATA
    def cleansing_content(self, row):
        """
        This function use for clean news content like html/css/js tags, punctuation, split camelcase, and many more
        """
        # define variable
        source = row[0]
        category = row[1]
        content_clean = row[2]

        # define regex for tokenize a words
        pattern = r'\w+' #for any word char
        pattern2 = r'.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)' #tokenize for camelcase

        # REMOVE WEBSITE URL FIRST
        content_clean = self.remove_source_url(content_clean)
        content_clean = self.remove_source_url2(content_clean)

        # cleansing with regex
        if 'carmudi' in source.lower():
            # CARMUDI
            # mendeteksi nama penulis di akhir kalimat (abc)
            # (\([a-zA-Z]+\)$)
            carmudi_pattern1 = r'(\([a-zA-Z]+\)$)'
            content_clean = re.sub(carmudi_pattern1, ' \n ', content_clean)
            # mendeteksi nama penulis di akhir kalimat (abc/abc)
            # (\([a-zA-Z]+\/[a-zA-Z]+\)$)
            carmudi_pattern2 = r'(\([a-zA-Z]+\/[a-zA-Z]+\)$)'
            content_clean = re.sub(carmudi_pattern2, ' \n', content_clean)

        if 'cintamobil' in source.lower():
            # CINTAMOBIL
            # mendeteksi dan menghapus link sisip (>>>...)
            content_clean = ' \n '.join([re.sub(r'(>>>.+)', ' ', x) for x in nltk.regexp_tokenize(content_clean, pattern2)])

        if 'detik' in source.lower():
            # DETIK
            # mendeteksi (abc/abc)
            # (\([a-zA-Z]+\/[a-zA-Z]+\)$)
            detik_pattern = r'(\([a-zA-Z]+\/[a-zA-Z]+\)$)'
            content_clean = re.sub(detik_pattern, ' \n ', content_clean)

        if 'kompas' in source.lower():
            # KOMPAS
            # mendeteksi baca juga
            kompas_pattern = r'(baca juga:.*?(\.|$))'
            content_clean = re.sub(kompas_pattern, ' \n ', content_clean)

        if 'metrotvnews' in source.lower():
            # METROTVNEWS
            # mendeteksi nama penulis di akhir paragraf (abc)
            metro_pattern = r'(\([a-zA-Z]+\)$)'
            content_clean = re.sub(metro_pattern, ' \n ', content_clean)

        if 'mobil123' in source.lower():
            # MOBIL123
            # mendeteksi nama penulis di akhir paragraf [abc/abc]
            mobil123_pattern1 = r'(\[([a-zA-Z])+\/[a-zA-Z]+\]$)'
            content_clean = re.sub(mobil123_pattern1, ' \n ', content_clean)

            # mendeteksi nama penulis di akhir paragraf [dol]
            mobil123_pattern2 = r'(\[([a-zA-Z])*\]$)'
            content_clean = re.sub(mobil123_pattern2, ' \n ', content_clean)

        if 'okezone' in source.lower():
            # OKEZONE
            # mendeteksi nama penulis di akhir paragraf (abc)
            okezone_pattern = r'(\([a-zA-Z]+\)$)'
            content_clean = re.sub(okezone_pattern, ' \n ', content_clean)

        if 'oto' in source.lower():
            # OTO
            # mendeteksi (dol) di akhir paragraf
            oto_pattern1 = r'(\([a-zA-Z]+\)$)'

            content_clean = re.sub(oto_pattern1, ' \n ', content_clean)
            # mendeteksi (abc/abc)
            oto_pattern2 = r'(\([a-z]+\/[a-z]+\)$)'
            content_clean = re.sub(oto_pattern2, ' \n ', content_clean)

            # mendeteksi [dol/dol] di akhir paragraf
            oto_pattern3 = r'(\[([a-zA-Z])+\/[a-zA-Z]+\]$)'
            content_clean = re.sub(oto_pattern3, ' \n ', content_clean)

            # mendeteksi [dol] di akhir paragraf
            oto_pattern4 = r'(\[([a-zA-Z])*\]$)'
            content_clean = re.sub(oto_pattern4, ' \n ', content_clean)

        if 'otosia' in source.lower():
            # OTOSIA
            # mendeteksi (dol) di akhir paragraf
            otosia_pattern1 = r'(\([a-zA-Z]+\)$)'
            content_clean = re.sub(otosia_pattern1, ' \n ', content_clean)

            # mendeteksi (abc/abc)
            otosia_pattern2 = r'(\([a-z]+\/[a-z]+\)$)'
            content_clean = re.sub(otosia_pattern2, ' \n ', content_clean)

            # mendeteksi (abc/abc/abc)
            otosia_pattern3 = r'(\([a-zA-Z]+\/[a-zA-Z]+\/[a-zA-Z]+\)$)'
            content_clean = re.sub(otosia_pattern3, ' \n ', content_clean)

            # mendeteksi (abc/abc/abc/abc)
            otosia_pattern4 = r'(\([a-zA-Z]+\/[a-zA-Z]+\/[a-zA-Z]+\/[a-zA-Z]+\)$)'
            content_clean = re.sub(otosia_pattern4, ' \n ', content_clean)

        if 'propertiterkini' in source.lower():
            # PROPERTITERKINI
            # mendeteksi penulis dan baca juga
            properti_pattern = r'(\[[a-zA-Z].+\/[a-zA-Z].+\]+Baca Juga.+$)'
            content_clean = re.sub(properti_pattern, ' \n ', content_clean)

        if 'rajamobil' in source.lower():
            # RAJAMOBIL
            # mendeteksi comment in script
            rajamobil_pattern1 = r'(&lt;span data-mce-type=\"bookmark\".+$)'
            content_clean = re.sub(rajamobil_pattern1, ' \n ', content_clean)

            # mendeteksi [dol] di akhir paragraf
            rajamobil_pattern2 = r'(\[([a-zA-Z])*\]$)'
            content_clean = re.sub(rajamobil_pattern2, ' \n ', content_clean)

            # mendeteksi [dol] di akhir paragraf + iklan
            rajamobil_pattern3 = r'(\[([a-zA-Z])*\].+$)'
            content_clean = re.sub(rajamobil_pattern3, ' \n ', content_clean)

        if 'rumahku' in source.lower():
            # RUMAHKU
            rumahku_pattern = r'(Untuk keperluan informasi dapat mengunjungi website:http://www.primesystem.id.+$)'
            content_clean = re.sub(rumahku_pattern, ' \n ', content_clean)

        if 'sindonews' in source.lower():
            # SINDONEWS
            # regex
            sindo_pattern = r'(\([a-zA-Z]+\)$)'
            content_clean = re.sub(sindo_pattern, ' \n ', content_clean)

        # remove punctuation and tokenize only words
        list_content_clean = nltk.regexp_tokenize(content_clean, pattern)

        # split camelcase word
        content_clean = ' '.join([' '.join(nltk.regexp_tokenize(word, pattern2)) for word in list_content_clean])

        return content_clean.lower()
