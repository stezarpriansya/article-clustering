import pandas as pd
import numpy as np
import os
# set working directory first
cwd = os.getcwd()
os.chdir("D:\\OJT FILES\\article-clustering\\Classification")

import DataPreparation as dp
dp_obj = dp.DataPreparation()

from nltk import word_tokenize
import nltk
import gensim.corpora as corpora
from gensim.models import Word2Vec
#--------LOAD DATA------------#
# test_data_cba = pd.read_csv('dataset/article_201809061411.csv')
# test_data_cba = test_data_cba[test_data_cba.source == 'seva']
# test_data_cba = test_data_cba[(test_data_cba.content.notnull()) & (test_data_cba.category.notnull())]
test_data_cba = pd.read_csv('dataset/test_seva.csv')
#---------Cleansing content-------------#
#remove character not necessary
def cleansing_pipeline(test_data):
    test_data['content'] = test_data[['source', 'category', 'content']].apply(dp_obj.cleansing_content, axis=1)
    # remove common trigrams in property category article
    test_data.loc[test_data.category=='properti', 'content'] = test_data.loc[test_data.category=='properti', 'content'].apply(lambda x : dp_obj.remove_trigrams(x))

    # tokenize
    test_data['tokenized'] = test_data['content'].apply(lambda x : word_tokenize(x))

    # remove stopwords
    test_data['tokenized'] = test_data['tokenized'].apply(lambda x : dp_obj.remove_stopwords(x))

    return test_data

test_data_cba = cleansing_pipeline(test_data_cba)
#-------SPLIT DATA BY CATEGORY----------#
test_data_oto = test_data_cba[test_data_cba.category == 'otomotif']
test_data_pro = test_data_cba[test_data_cba.category == 'properti']

#-------LOAD WORD2VEC MODEL------------#
model = Word2Vec.load('model/word2vec/word2vec1.model')
model.wv.save_word2vec_format('model_w2v1.bin',binary=True)
# TAMBAHAN : load data seva after cleansing punya Nella
# test_data = pd.read_csv('dataset/predict_oto2.csv', index_col=0)
test_data_filter_oto = test_data_oto[['id','tokenized', 'category', 'subcategory']]
# test_data_cba.head(
# 1-jaccard_distance(set('aku'), set('aku'))
# model.wv.similarity(w1='aku', w2='aku')
#---------- DEF CLASSIFICAION -------------#
def do_classificaion(keywords, tokens, model):
    from nltk.metrics import edit_distance
    hasil = {}
    for col in keywords.columns:
        total_key = len(keywords[col].tolist())
        list_scores = []
        sum_score_col = 0.0
        avg_score_col = 0.0
        total_tokens = sum([x[1] for x in tokens])
        total_data = len(keywords[col].tolist()) * len(tokens)
        for key in keywords[col].tolist():
            key = key.split(',')
            for token in tokens:
                score_key = int(key[1])/total_key
                score_token = token[1]/total_tokens
                # similarity syntax between two different words
                score_jacc = edit_distance(token[0], key[0])/max(len(token[0]), len(key[0]))
                # similarity context between two different words
                try:
                    score_w2v = model.wv.similarity(w1=token[0], w2=key[0])
                except:
                    # if the tokens is not in w2v vocab
                    score_w2v = 0.0
                    # score = 0.0
                # sum_score_col = sum_score_col + (((score * (token[1]/total_tokens) * (int(key[1])/total_key))*0.9) + ((1-jaccard_distance(set(token[0]), set(key[0]))) * (token[1]/total_tokens) * (int(key[1])/total_key) * 0.1))
                # sum_score_col = sum_score_col + (score * (token[1]/total_tokens) * (int(key[1])/total_key))
                sum_score_col = sum_score_col + ((score_w2v * 0.5) + (score_key * 0.2) + (score_token * 0.2) + (score_jacc * 0.1))
        avg_score_col = sum_score_col / total_data
        list_scores.append(avg_score_col)
        list_scores.append(sum_score_col)
        hasil[col] = list_scores
    return hasil

#------------- VOTING LABELLING FROM RESULT DICT -------------#
def vote_label(hasil):
    temp_df = pd.DataFrame(hasil).transpose()
    # sort descending then take first
    ret_df = temp_df.sort_values(by=[1], ascending=False).iloc[0]
    # return class, avg_score, sum_score
    return ret_df.name

#---------- 6 KEYWORDS VALID-----------#
# (1-jaccard_distance(set('pengharum'), set('tips'))) * 0.2
# model.wv.similarity(w1='pengharum', w2='tips') * 0.8
keywords6 = pd.read_csv('dataset/keyword_baru.csv')
keywords6.columns = ['0','1','2','3','4','5']
# keywords6.head()
# test_data_filter_oto_2.iloc[0]['tokenized_freq'].most_common()
# Drop kategori olahraga/motogp karena tidak relevan dengan seva
# keywords7.drop(['3'], axis=1, inplace=True)
#---------- DO CLASSIFICAION -------------#
test_data_filter_oto_2 = test_data_filter_oto.copy()
test_data_filter_oto_2.loc[test_data_filter_oto_2['subcategory'] == 'tips-rekomendasi', 'sub'] = '4'
test_data_filter_oto_2.loc[test_data_filter_oto_2['subcategory'] == 'modifikasi', 'sub'] = '2'
test_data_filter_oto_2.loc[test_data_filter_oto_2['subcategory'] == 'hobi-komunitas', 'sub'] = '0'
test_data_filter_oto_2.loc[test_data_filter_oto_2['subcategory'] == 'review-otomotif', 'sub'] = '3'
test_data_filter_oto_2.loc[test_data_filter_oto_2['subcategory'] == 'travel-lifestyle', 'sub'] = '5'
test_data_filter_oto_2.loc[test_data_filter_oto_2['subcategory'] == 'keuangan', 'sub'] = '1'
test_data_filter_oto_2.shape
test_data_filter_oto_2 = test_data_filter_oto_2[(test_data_filter_oto_2.subcategory != 'berita-terbaru') & (test_data_filter_oto_2.subcategory != 'berita-otomotif')]

test_data_filter_oto_2['tokenized_freq'] = test_data_filter_oto_2['tokenized'].apply(lambda x: nltk.FreqDist(ch for ch in x))
# test_data_filter_oto_2['tokenized'] = test_data_filter_oto_2['tokenized'].apply(lambda x : set([y for y in x.split(',') if y.isalpha()]))
# test_data_filter['tokenized'] = test_data_filter['tokenized'].apply(lambda x :set(x))
test_data_filter_oto_2['hasil_w2v'] = test_data_filter_oto_2['tokenized_freq'].apply(lambda tokens : do_classificaion(keywords6, tokens.most_common(), model))
test_data_filter_oto_2['predict_w2v'] = test_data_filter_oto_2['hasil_w2v'].apply(lambda hasil : vote_label(hasil))
#--------- CALCULATE ACCURACY--------#
test_data_filter_oto_2.head()
# build confussion matrix
from sklearn.metrics import confusion_matrix
cm7 = confusion_matrix(test_data_filter_oto_2['sub'].tolist(), test_data_filter_oto_2['predict_w2v'].tolist())
test_data_filter_oto_2['val'] = 0
test_data_filter_oto_2.loc[test_data_filter_oto_2['sub'] == test_data_filter_oto_2['predict_w2v'], 'val'] = 1
# test_data_filter01 = test_data_filter1[(test_data_filter1['sub'] != '0') & (test_data_filter1['sub'] != '1')]
accuracy_score6 = (test_data_filter_oto_2[test_data_filter_oto_2['val'] == 1].shape[0] / test_data_filter_oto_2.shape[0]) * 100
accuracy_score6
test_data_filter2.groupby('subcategory').count()['predict_w2v']
test_data_filter2[test_data_filter2['val'] == 1].groupby('subcategory').count()['predict_w2v']
test_data_filter2[test_data_filter2['val'] == 0].groupby('subcategory').count()['predict_w2v']
test_data_filter2[test_data_filter2['subcategory'] == 'travel-lifestyle']

#------ KEYWORDS 9 --------#
keywords = pd.read_csv('dataset/keywords_oto_9.csv', index_col=0)
# Drop kategori olahraga/motogp karena tidak relevan dengan seva
keywords.drop(['3'], axis=1, inplace=True)
keywords.head()
#---------- DO CLASSIFICAION -------------#
test_data_filter1 = test_data_filter.copy()
test_data_filter1.loc[test_data_filter1['subcategory'] == 'berita-otomotif', 'sub'] = '0'
test_data_filter1.loc[test_data_filter1['subcategory'] == 'tips-rekomendasi', 'sub'] = '1'
test_data_filter1.loc[test_data_filter1['subcategory'] == 'modifikasi', 'sub'] = '2'
test_data_filter1.loc[test_data_filter1['subcategory'] == 'hobi-komunitas', 'sub'] = '4'
test_data_filter1.loc[test_data_filter1['subcategory'] == 'review-otomotif', 'sub'] = '5'
test_data_filter1.loc[test_data_filter1['subcategory'] == 'travel-lifestyle', 'sub'] = '6'
test_data_filter1.loc[test_data_filter1['subcategory'] == 'berita-terbaru', 'sub'] = '7'
test_data_filter1.loc[test_data_filter1['subcategory'] == 'keuangan', 'sub'] = '8'

test_data_filter1['tokenized'] = test_data_filter1['tokenized'].apply(lambda x :set([y for y in x.split(',') if y.isalpha()]))
# test_data_filter['tokenized'] = test_data_filter['tokenized'].apply(lambda x :set(x))
test_data_filter1['hasil_w2v'] = test_data_filter1['tokenized'].apply(lambda tokens : do_classificaion(keywords, tokens, model))
test_data_filter1['predict_w2v'] = test_data_filter1['hasil_w2v'].apply(lambda hasil : vote_label(hasil))
#--------- CALCULATE ACCURACY--------#
# build confussion matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(test_data_filter1['sub'].tolist(), test_data_filter1['predict_w2v'].tolist())
test_data_filter1['val'] = 0
test_data_filter1.loc[test_data_filter1['sub'] == test_data_filter1['predict_w2v'], 'val'] = 1
# test_data_filter01 = test_data_filter1[(test_data_filter1['sub'] != '0') & (test_data_filter1['sub'] != '1')]
accuracy_score = (test_data_filter1[test_data_filter1['val'] == 1].shape[0] / test_data_filter1.shape[0]) * 100
accuracy_score
test_data_filter1.groupby('subcategory').count()['predict_w2v']
test_data_filter1[test_data_filter1['val'] == 1].groupby('subcategory').count()['predict_w2v']
test_data_filter1[test_data_filter1['val'] == 0].groupby('subcategory').count()['predict_w2v']
test_data_filter1[(test_data_filter1['val'] == 0) & (test_data_filter1['subcategory'] == 'berita-otomotif')]
test_data_filter1.loc[test_data_filter1['id'] == 917, 'tokenized'].tolist()
print(test_data.loc[test_data['id'] == 917, 'content'])

#---------- 7 KEYWORDS -----------#
keywords7 = pd.read_csv('dataset/keywords_oto_7.csv', index_col=0)
# Drop kategori olahraga/motogp karena tidak relevan dengan seva
keywords7.drop(['3'], axis=1, inplace=True)
#---------- DO CLASSIFICAION -------------#
test_data_filter2 = test_data_filter.copy()
test_data_filter2.loc[test_data_filter2['subcategory'] == 'tips-rekomendasi', 'sub'] = '1'
test_data_filter2.loc[test_data_filter2['subcategory'] == 'modifikasi', 'sub'] = '2'
test_data_filter2.loc[test_data_filter2['subcategory'] == 'hobi-komunitas', 'sub'] = '4'
test_data_filter2.loc[test_data_filter2['subcategory'] == 'review-otomotif', 'sub'] = '5'
test_data_filter2.loc[test_data_filter2['subcategory'] == 'travel-lifestyle', 'sub'] = '6'
test_data_filter2.loc[test_data_filter2['subcategory'] == 'keuangan', 'sub'] = '0'
test_data_filter2.shape
test_data_filter2 = test_data_filter2[(test_data_filter2.subcategory != 'berita-terbaru') & (test_data_filter2.subcategory != 'berita-otomotif')]

test_data_filter2['tokenized'] = test_data_filter2['tokenized'].apply(lambda x : set([y for y in x.split(',') if y.isalpha()]))
# test_data_filter['tokenized'] = test_data_filter['tokenized'].apply(lambda x :set(x))
test_data_filter2['hasil_w2v'] = test_data_filter2['tokenized'].apply(lambda tokens : do_classificaion(keywords7, tokens, model))
test_data_filter2['predict_w2v'] = test_data_filter2['hasil_w2v'].apply(lambda hasil : vote_label(hasil))
#--------- CALCULATE ACCURACY--------#
# build confussion matrix
cm7 = confusion_matrix(test_data_filter2['sub'].tolist(), test_data_filter2['predict_w2v'].tolist())
test_data_filter2['val'] = 0
test_data_filter2.loc[test_data_filter2['sub'] == test_data_filter2['predict_w2v'], 'val'] = 1
# test_data_filter01 = test_data_filter1[(test_data_filter1['sub'] != '0') & (test_data_filter1['sub'] != '1')]
accuracy_score7 = (test_data_filter2[test_data_filter2['val'] == 1].shape[0] / test_data_filter2.shape[0]) * 100
accuracy_score7
test_data_filter2.groupby('subcategory').count()['predict_w2v']
test_data_filter2[test_data_filter2['val'] == 1].groupby('subcategory').count()['predict_w2v']
test_data_filter2[test_data_filter2['val'] == 0].groupby('subcategory').count()['predict_w2v']
test_data_filter2[test_data_filter2['subcategory'] == 'travel-lifestyle']
