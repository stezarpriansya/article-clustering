import pandas as pd
import numpy as np
import os
# set working directory first
cwd = os.getcwd()
os.chdir("D:\\OJT FILES\\article-clustering\\Classification")

import DataPreparation as dp
dp_obj = dp.DataPreparation()

from nltk import word_tokenize
import nltk
import gensim.corpora as corpora
from gensim.models import Word2Vec
#--------LOAD DATA------------#
# test_data_cba = pd.read_csv('dataset/article_201809061411.csv')
# test_data_cba = test_data_cba[test_data_cba.source == 'seva']
# test_data_cba = test_data_cba[(test_data_cba.content.notnull()) & (test_data_cba.category.notnull())]
test_data_cba = pd.read_csv('dataset/test_seva.csv')
test_data_cba.shape
#---------Cleansing content-------------#
#remove character not necessary
def cleansing_pipeline(test_data):
    test_data['content'] = test_data[['source', 'category', 'content']].apply(dp_obj.cleansing_content, axis=1)
    # remove common trigrams in property category article
    test_data.loc[test_data.category=='properti', 'content'] = test_data.loc[test_data.category=='properti', 'content'].apply(lambda x : dp_obj.remove_trigrams(x))

    # tokenize
    test_data['tokenized'] = test_data['content'].apply(lambda x : word_tokenize(x))

    # remove stopwords
    test_data['tokenized'] = test_data['tokenized'].apply(lambda x : dp_obj.remove_stopwords(x))

    return test_data

test_data_cba = cleansing_pipeline(test_data_cba)
#-------SPLIT DATA BY CATEGORY----------#
test_data_oto = test_data_cba[test_data_cba.category == 'otomotif']
test_data_pro = test_data_cba[test_data_cba.category == 'properti']

#-------LOAD WORD2VEC MODEL------------#
model = Word2Vec.load('model/word2vec/model_w2v_wiki_berita')
model.wv.save_word2vec_format('model_w2v_wiki_berita.bin',binary=True)
# model = Word2Vec.load('model/word2vec/model_w2v_wiki_berita_sg')
# TAMBAHAN : load data seva after cleansing punya Nella
# test_data = pd.read_csv('dataset/predict_oto2.csv', index_col=0)
test_data_filter_oto = test_data_oto[['id','tokenized', 'category', 'subcategory']]
# model.wv.similarity(w1='aku', w2='kamu')
dict_w2v = set(model.wv.index2word)
# ws1 = ['knalpot', 'roda', 'kamu', 'dia', 'roda']
# ws1 = [x for x in ws1 if (x.isalpha()) & (x in dict_w2v)]
#
# ws2 = ['motor', 'tips', 'oli', 'mobil']
# ws2 = [x for x in ws2 if (x.isalpha()) & (x in dict_w2v)]
# model.wv.n_similarity(ws1 = ws1, ws2 = ws2)
# model.wv.distances(ws1, ws2)
# ws1
# test_data_cba.head(
# 1-jaccard_distance(set('aku'), set('aku'))
# model.wv.similarity(w1='aku', w2='aku')
#---------- DEF CLASSIFICAION -------------#
def do_classificaion2(keywords, tokens, model, dict):
    """unique Token with occurence"""
    hasil = {}
    for col in keywords.columns:
        total_key = len(keywords[col].tolist())
        list_scores = []
        sum_score_col = 0.0
        total_tokens = sum([x[1] for x in tokens])
        tokens = [x for x in tokens if (x[0].isalpha()) & (x[0] in dict_w2v)]
        ws1 = [x[0] for x in tokens]
        keywords_awal = [x.split(',') for x in keywords[col].tolist()]
        ws2 = [x[0] for x in keywords_awal if x[0] in dict_w2v]
        occ_ws1 = [x[1] for x in tokens]
        score_w2v = model.wv.n_similarity(ws1, ws2)
        sum_score_col = sum_score_col + score_w2v
        list_scores.append(sum_score_col)
        hasil[col] = list_scores
    return hasil

def do_classificaion3(keywords, tokens, model, dict):
    """Non unique Token"""
    hasil = {}
    for col in keywords.columns:
        total_key = len(keywords[col].tolist())
        list_scores = []
        sum_score_col = 0.0
        ws1 = [x for x in tokens if (x.isalpha()) & (x in dict_w2v)]
        keywords_awal = [x.split(',') for x in keywords[col].tolist()]
        ws2 = [x[0] for x in keywords_awal if x[0] in dict_w2v]
        score_w2v = model.wv.n_similarity(ws1, ws2)
        sum_score_col = sum_score_col + score_w2v
        list_scores.append(sum_score_col)
        hasil[col] = list_scores
    return hasil

def do_classificaion4(keywords, tokens, model, dict):
    """Non unique Token ditambah similarity syntax"""
    hasil = {}
    for col in keywords.columns:
        total_key = len(keywords[col].tolist())
        list_scores = []
        score_syntax = 0.0
        ws1 = [x for x in tokens if (x.isalpha()) & (x in dict_w2v)]
        ws1_non = [x for x in tokens if (x.isalpha()) & (x not in dict_w2v)] # not in w2c
        keywords_awal = [x.split(',') for x in keywords[col].tolist()]
        ws2 = [x[0] for x in keywords_awal if x[0] in dict_w2v]
        # ws2_non = set([x[0] for x in keywords_awal if x[0] not in dict_w2v]) # not in w2c
        if len(ws1_non) != 0:
            score_syntax = sum([1.0 for x in ws1_non if x in set(ws2)])/(len(ws1_non) * len(ws2))

        score_w2v = model.wv.n_similarity(ws1, ws2)
        sum_score_col = (score_w2v * 0.9) + (score_syntax * 0.1)
        list_scores.append(sum_score_col)
        hasil[col] = list_scores
    return hasil

#------------- VOTING LABELLING FROM RESULT DICT -------------#
def vote_label2(hasil):
    temp_df = pd.DataFrame(hasil).transpose()
    # sort descending then take first
    ret_df = temp_df.sort_values(by=[0], ascending=False).iloc[0]
    # return class, avg_score, sum_score
    return ret_df.name

#---------- 6 KEYWORDS VALID-----------#
keywords6 = pd.read_csv('dataset/keyword_baru.csv')
keywords6.columns = ['0','1','2','3','4','5']
#---------- DO CLASSIFICAION -------------#
test_data_filter_oto_2 = test_data_filter_oto.copy()
test_data_filter_oto_2.loc[test_data_filter_oto_2['subcategory'] == 'tips-rekomendasi', 'sub'] = '4'
test_data_filter_oto_2.loc[test_data_filter_oto_2['subcategory'] == 'modifikasi', 'sub'] = '2'
test_data_filter_oto_2.loc[test_data_filter_oto_2['subcategory'] == 'hobi-komunitas', 'sub'] = '0'
test_data_filter_oto_2.loc[test_data_filter_oto_2['subcategory'] == 'review-otomotif', 'sub'] = '3'
test_data_filter_oto_2.loc[test_data_filter_oto_2['subcategory'] == 'travel-lifestyle', 'sub'] = '5'
test_data_filter_oto_2.loc[test_data_filter_oto_2['subcategory'] == 'keuangan', 'sub'] = '1'
test_data_filter_oto_2.shape
test_data_filter_oto_2 = test_data_filter_oto_2[(test_data_filter_oto_2.subcategory != 'berita-terbaru') & (test_data_filter_oto_2.subcategory != 'berita-otomotif')]
test_data_filter_oto_2['tok'] = test_data_filter_oto_2['tokenized'].apply(lambda x:len(x))
sum(test_data_filter_oto_2['tok'].tolist())
# test_data_filter_oto_2['tokenized_freq'] = test_data_filter_oto_2['tokenized'].apply(lambda x: nltk.FreqDist(ch for ch in x))
# test_data_filter_oto_2['tokenized'] = test_data_filter_oto_2['tokenized'].apply(lambda x : set([y for y in x.split(',') if y.isalpha()]))
# test_data_filter['tokenized'] = test_data_filter['tokenized'].apply(lambda x :set(x))
# test_data_filter_oto_2['hasil_w2v'] = test_data_filter_oto_2['tokenized_freq'].apply(lambda tokens : do_classificaion2(keywords6, tokens.most_common(), model, dict_w2v))
%time test_data_filter_oto_2['hasil_w2v'] = test_data_filter_oto_2['tokenized'].apply(lambda tokens : do_classificaion3(keywords6, tokens, model, dict_w2v))
test_data_filter_oto_2['predict_w2v'] = test_data_filter_oto_2['hasil_w2v'].apply(lambda hasil : vote_label2(hasil))
#--------- CALCULATE ACCURACY--------#
# build confussion matrix
# from sklearn.metrics import confusion_matrix
# cm7 = confusion_matrix(test_data_filter_oto_2['sub'].tolist(), test_data_filter_oto_2['predict_w2v'].tolist())
test_data_filter_oto_2['val'] = 0
test_data_filter_oto_2.loc[test_data_filter_oto_2['sub'] == test_data_filter_oto_2['predict_w2v'], 'val'] = 1
accuracy_score6 = (test_data_filter_oto_2[test_data_filter_oto_2['val'] == 1].shape[0] / test_data_filter_oto_2.shape[0]) * 100
accuracy_score6
tp = test_data_filter_oto_2[test_data_filter_oto_2.val == 1].groupby('subcategory').count()[['id']]
fp = test_data_filter_oto_2[test_data_filter_oto_2.val == 0].groupby('subcategory').count()[['id']]
all = test_data_filter_oto_2.groupby('subcategory').count()[['id']]
presentasi_per_class = pd.merge(tp, fp, on='subcategory', how='inner')
presentasi_per_class = pd.merge(presentasi_per_class, all, on='subcategory', how='inner')
presentasi_per_class['persen_tp'] = presentasi_per_class.id_x / presentasi_per_class.id
presentasi_per_class['persen_fp'] = presentasi_per_class.id_y / presentasi_per_class.id
presentasi_per_class
