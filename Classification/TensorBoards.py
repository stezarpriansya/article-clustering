import pandas as pd
import numpy as np
import os
# set working directory first
cwd = os.getcwd()
os.chdir("D:\\OJT FILES\\article-clustering\\Classification")
from gensim.models import Word2Vec

# BUAT TensorBoard
import io
# import gensim
import tensorflow as tf
from tensorflow.contrib.tensorboard.plugins import projector
gensim_model = Word2Vec.load('model/word2vec/model_w2v_wiki_berita')
# gensim_model.save('logdir\\WikiBeritaStezar\\gensim_model.cpkt')
model_folder = 'logdir\\WikiBeritaStezar'
weights = gensim_model.wv.syn0
idx2words = gensim_model.wv.index2word

vocab_size = weights.shape[0]
embedding_dim = weights.shape[1]

with io.open("logdir\\WikiBeritaStezar\\metadata.tsv", 'w+', encoding='utf8') as f:
    f.writelines("\n".join(idx2words))

tf.reset_default_graph()

W = tf.Variable(tf.constant(0.0, shape=[vocab_size, embedding_dim]), trainable=False, name="W")
embedding_placeholder = tf.placeholder(tf.float32, [vocab_size, embedding_dim])
embedding_init = W.assign(embedding_placeholder)

writer = tf.summary.FileWriter(model_folder, graph=tf.get_default_graph())
saver = tf.train.Saver()
# Format: tensorflow/contrib/tensorboard/plugins/projector/projector_config.proto
config = projector.ProjectorConfig()

# You can add multiple embeddings. Here we add only one.
embedding = config.embeddings.add()
embedding.tensor_name = W.name
embedding.metadata_path = 'metadata.tsv'
# Saves a configuration file that TensorBoard will read during startup.
projector.visualize_embeddings(writer, config)

with tf.Session() as sess:
    sess.run(embedding_init, feed_dict={embedding_placeholder: weights})
    save_path = saver.save(sess, "logdir\\WikiBeritaStezar\\tf-model.cpkt")
###################
