import logging, gensim
info = logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
from gensim import corpora
from gensim import corpora, models, similarities
from gensim.models import ldamodel
from collections import defaultdict

text = "petrokimia gresik mengadakan recruitment secara besar-besaran bulan November"

stoplist = {"akan", "sama", "bagai", "lalu", "mengenai", "dari pada", "di", "ke", "dari", "hingga", "sampai", "bagi", 
            "untuk", "guna", "buat", "dengan", "sambil", "beserta", "bersama", "karena", "sebab", "lantaran", "oleh", "tentang", 
            "dan", "serta", "atau", "tetapi", "melainkan", "padahal", "sedangkan", "sejak", "semenjak", 
            "sedari", "sewaktu", "ketika", "tatkala", "selama", "demi", "serta", "hingga", "sampai", "setelah", "sesudah", 
            "sebelum", "selesai", "seusai", "sehabis", "jika", "kalau", "jikalau", "bila", "manakala", "andai", "umpama", 
            "biarpun", "sekalipun", "seakan-akan", "seperti", "sebagai", "sebab", "karena", "oleh sebab", "sehingga", "sampai", 
            "dengan", "tanpa", "bahwa", "yang", "sama dengan", "lebih dari", "bah", "cih", "cis", "ih", "idih", "brengsek", 
            "sialan", "buset", "keparat", "aduhai", "amboi", "asyik", "syukur", "alhamdulillah", "insya allah", "aduh", "aih", 
            "ai", "lo", "duilah", "eh", "oh", "ah", "astaga", "astagfirullah", "masyaallah", "ayo", "mari", "hai", "be", "eh", 
            "halo", "nah", "sang", "sri", "hang", "dang", "si", "bagaimana", "mengapa", "apa", "siapa", "dimana", "kapan", 
            "jadi", "tanya", "dalam", "http", "ini", "itu", "com", "ingin", "ha", "lebih", "rp", "rupa", "adalah", 
            "pada", "bapak", "ibu", "bpk", "bu", "minta", "mohon", "bagaimana", "apakah", "mengapa", "dimana", "kenapa", "siapa"}

uji = [word for word in text.lower().split() if word not in stoplist]

lda_model = gensim.models.LdaModel.load('35_1.model')
sictionary = gensim.corpora.Dictionary.load('dictionary.dict')

# transform text into the bag-of-words space
bow_vector = dictionary.doc2bow([text for text in uji])
# transform into LDA space
lda_vector = lda_model[bow_vector]
print(max(lda_vector, key=lambda item: item[1])[0])